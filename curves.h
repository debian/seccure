/*
 *  seccure  -  Copyright 2014 B. Poettering
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License
 *  as published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

/* 
 *   SECCURE Elliptic Curve Crypto Utility for Reliable Encryption
 *
 *              http://point-at-infinity.org/seccure/
 *
 *
 * seccure implements a selection of asymmetric algorithms based on  
 * elliptic curve cryptography (ECC). See the manpage or the project's  
 * homepage for further details.
 *
 * This code links against the GNU gcrypt library "libgcrypt" (which
 * is part of the GnuPG project). Use the included Makefile to build
 * the binary.
 * 
 * Report bugs to: seccure AT point-at-infinity.org
 *
 */

#ifndef INC_CURVES_H
#define INC_CURVES_H

#include "ecc.h"

#define MAX_PK_LEN_BIN 66
#define MAX_PK_LEN_COMPACT 81
#define MAX_SIG_LEN_BIN 131
#define MAX_SIG_LEN_COMPACT 161
#define MAX_DH_LEN_BIN 32
#define MAX_DH_LEN_COMPACT 40
#define MAX_ELEM_LEN_BIN 66
#define MAX_ORDER_LEN_BIN 66

struct curve_params {
  const char *name;
  struct domain_params dp;
  size_t pk_len_bin, pk_len_compact;
  size_t sig_len_bin, sig_len_compact;
  size_t dh_len_bin, dh_len_compact;
  size_t elem_len_bin, order_len_bin;
};

struct curve_params* curve_by_name(const char *name);
struct curve_params* curve_by_pk_len_compact(size_t len);
void curve_release(struct curve_params *cp);

#endif /* INC_CURVES_H */
