/*
 *  seccure  -  Copyright 2014 B. Poettering
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public License
 *  as published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this program. If not, see
 *  <http://www.gnu.org/licenses/>.
 */

/* 
 *   SECCURE Elliptic Curve Crypto Utility for Reliable Encryption
 *
 *              http://point-at-infinity.org/seccure/
 *
 *
 * seccure implements a selection of asymmetric algorithms based on  
 * elliptic curve cryptography (ECC). See the manpage or the project's  
 * homepage for further details.
 *
 * This code links against the GNU gcrypt library "libgcrypt" (which
 * is part of the GnuPG project). Use the included Makefile to build
 * the binary.
 * 
 * Report bugs to: seccure AT point-at-infinity.org
 *
 */

#ifndef INC_SERIALIZE_H
#define INC_SERIALIZE_H

#include <gcrypt.h>

enum disp_format { DF_BIN, DF_COMPACT };

#define COMPACT_DIGITS_COUNT 90
extern const char compact_digits[];

size_t get_serialization_len(const gcry_mpi_t x, enum disp_format df);
void serialize_mpi(char *outbuf, size_t outlen, enum disp_format df, 
		   const gcry_mpi_t x);
int deserialize_mpi(gcry_mpi_t *x, enum disp_format ds, const char *buf, 
		    size_t inlen);

#endif /* INC_SERIALIZE_H */
